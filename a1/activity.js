//a. What directive is used by Node.js in loading the modules it needs?
  /* 
  Answer:
    Store the package in variables and use require() method

    require() method
  */

//b. What Node.js module contains a method for server creation?
 /*
 Answer:
    HTTP
 */

//c. What is the method of the http object responsible for creating a server using Node.js?
 /*
 Answer:
    createServer() method
 */
//d. What method of the response object allows us to set status codes and content types?
/*
 Answer:
    reponse.writeHead() method
 */
//e. Where will console.log() output its contents when run in Node.js?
/*
Answer:
    Terminal / Git bash 
*/

//f. What property of the request object contains the address' endpoint?
/*
Answer: 
 localhost
*/


const http = require('http');


const port = 3000;

const server = http.createServer((request,response)=>{

    if(request.url == '/logIn'){
        response.writeHead(200, {'Content-Type':'text/plain'})
        response.end('Welcome to log In page!')
    }
    else if(request.url == '/register'){
        response.writeHead(200, {'Content-Type':'text/plain'})
        response.end('Welcom to register page!')
    }else{
        response.writeHead(404, {'Content-Type':'text/plain'})
        response.end('404 Not Found')
    }
})
server.listen(port)

console.log(`Server is now accessible at localhost:${port}`)