let http = require("http");

//require is a built in JS method which allow us to import packages
    //Packages are pieces of code we can integrate into our apllication

//HTTP is a default package that comes from Nodejs. It allows us to uses methods that let us create SERVES
//http is a module . Modules are package we imported
//Modules are object that contain codes, methods, or data

//protocol to client server communication - http://localhost:4000 - server/applications
http.createServer(function(request,response){
/*
    createServer() method is a method from the http module that allow us to handle request and response from a client and a srver respectively

    the request obj contains details of the request from the client 

    the response obj  contains details fpr the responsse from the server

    the createServer() method always  recieves the request objecyt frist before the response
*/
    
/*
    response.writeHead() - is a method of the response obj. It allows us to add:

    Headers - headers are additional infromation about our response
    We have 2 arguments in pur writHead() methods: (1) HTTP status code

    HTTP status is just a numerical code that let the client know about the status of their request 

    200- me3ans ok
    404- means the request cannot found
    (2) "Content-Type"-is one of the mmost recognizable headers. It simply  pertains to the data type
    of our response
*/
/*
response.end() - ends our respon */

    response.writeHead(200,{'Content-Type':'text/plain'});
    response.end("Hello, Alpha, from your first sample Server!");
}).listen(5000);

console.log("Server is running on localhost:5000!");

/*
    .listen() allows us toassign a port to our server

    This will allow us server our file (simple.js) server in our local ma
*/