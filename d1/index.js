/*
what is a client
 a client is an application which creates request for RESOURCES from a server. A client will trigger an action, in the web development context, through a URL and wait for the response of the server

 What is a server?
 A server is able to host a deliver RESOUCES request by a client.
 In fact, a single server can handle multiple clients.

 What is Node.js?

 Nodejs is a runtime environment which allows us to create/develop backend/server-side applications with JavaScript. Because by default, JS was conceptualized soley to the front end.
 Runtime Environment - is the environment in which  a program or application is executed

 Why is Nodejs Popular?
 Perfomance- Nodejs is one of the most pewrforming environment for creating backenf application with JS

 Familiarity - Since Nodejs is built and use JS as its language, it is very familiar for most developers

 NPM - node package Manager- is the larget registry for node packages
 Packages- are bits of programs, methods, functions, codes that gratly help int the development of an appliccation

*/


const http = require('http');


const port = 4000;

const server = http.createServer((request,response)=>{

    if(request.url == '/greeting'){
        response.writeHead(200, {'Content-Type':'text/plain'})
        response.end('Welcome to 248 2048 app')
    }
    else if(request.url == '/homepage'){
        response.writeHead(200, {'Content-Type':'text/plain'})
        response.end('This is the homepage!')
    }else{
        response.writeHead(404, {'Content-Type':'text/plain'})
        response.end('404 Not Found')
    }
})
server.listen(port)

console.log(`Server is now accessible at localhost:${port}`)